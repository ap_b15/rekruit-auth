package com.advprog2021.b15.rekruitauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RekruitAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(RekruitAuthApplication.class, args);
	}

}
